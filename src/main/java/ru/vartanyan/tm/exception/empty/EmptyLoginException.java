package ru.vartanyan.tm.exception.empty;

public class EmptyLoginException extends Exception {

    public EmptyLoginException() throws Exception {
        super("Error! Login cannot be null or empty...");
    }

}
