package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.model.Project;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

}
