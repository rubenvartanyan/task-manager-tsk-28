package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskUnbindByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-unbind-by-project-id";
    }

    @Override
    public String description() {
        return "Unbind task by project Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("BIND TASK TO PROJECT BY PROJECT ID");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER PROJECT ID]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        @NotNull final String taskId = TerminalUtil.nextLine();
        serviceLocator.getProjectTaskService().bindTaskByProjectId(projectId, taskId, userId);
        System.out.println("TASK ADDED TO PROJECT");
    }

}
