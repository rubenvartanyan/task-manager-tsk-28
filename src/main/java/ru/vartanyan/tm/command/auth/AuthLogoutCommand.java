package ru.vartanyan.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractAuthCommand;

public class AuthLogoutCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "Logout";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

}
