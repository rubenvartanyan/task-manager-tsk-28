package ru.vartanyan.tm.command.data;

import org.eclipse.persistence.jaxb.JAXBContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.io.File;
import ru.vartanyan.tm.dto.Domain;
import ru.vartanyan.tm.enumerated.Role;
import javax.xml.bind.Unmarshaller;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand{

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-jaxb-load";
    }

    @Nullable
    @Override
    public String description() {
        return "Load data from JSON jabx";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON LOAD]");
        System.out.println("[DATA JSON LOAD]");
        System.setProperty(JAVAX_XML_PROPERTY, ORG_ECLIPSE_PROPERTY);
        final File file = new File(FILE_JAXB_JSON);
        final JAXBContext jaxbContext = (JAXBContext) JAXBContext.newInstance(Domain.class);
        final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(ECLIPSELINK_PROPERTY, APPLICATION_JSON_PROPERTY);
        final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
