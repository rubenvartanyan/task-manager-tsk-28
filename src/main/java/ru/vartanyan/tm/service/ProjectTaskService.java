package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.service.IProjectTaskService;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService{

    @NotNull final private ITaskRepository taskRepository;

    @NotNull final private IProjectRepository projectRepository;

    public ProjectTaskService(@NotNull final ITaskRepository taskRepository,
                              @NotNull final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public final List<Task> findAllTaskByProjectId(@Nullable final String projectId,
                                             @NotNull final String userId) throws Exception{
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(projectId, userId);
    }

    @Override
    public void bindTaskByProjectId(@Nullable final String projectId,
                                    @Nullable final String taskId,
                                    @NotNull final String userId) throws Exception{
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        taskRepository.bindTaskByProjectId(projectId, taskId, userId);
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String projectId,
                                      @Nullable final String taskId,
                                      @NotNull final String userId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        taskRepository.unbindTaskFromProject(projectId, taskId, userId);
    }

    @Override
    public void removeProjectById(@Nullable final String projectId,
                                  @NotNull final String userId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(projectId, userId);
        projectRepository.removeById(projectId, userId);
    }

}
